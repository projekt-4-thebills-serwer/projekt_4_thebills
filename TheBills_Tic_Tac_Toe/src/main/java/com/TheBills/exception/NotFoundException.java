package com.TheBills.exception;

/**
 *  Wyświetla informacje dotyczącą braku możliwości połączenia z grą
 */
public class NotFoundException extends Exception {
    private String message;

    public NotFoundException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
