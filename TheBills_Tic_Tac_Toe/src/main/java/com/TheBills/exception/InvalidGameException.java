package com.TheBills.exception;

/**
 * Wyświetla informacje na temat przerwanej gry lub po jej zakończeniu
 */
public class InvalidGameException extends Exception {
    private String message;

    public InvalidGameException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
