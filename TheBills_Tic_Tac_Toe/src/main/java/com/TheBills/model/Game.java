package com.TheBills.model;

import lombok.Data;

/**
 * @Data generuje gettery, settery, metody toString oraz hashCode
 * Klasa przechowuje ID gry, nazwę obydwu graczy, status gry, planszę oraz zwycięzcę
 */
@Data
public class Game {

    private String gameId;
    private Player player1;
    private Player player2;
    private GameStatus status;
    private int[][] board;
    private TicToe winner;

}
