package com.TheBills.model;

/**
 * Enum to typ wyliczeniowy, który umożliwia w Javie zadeklarowanie ograniczonej liczby możliwych wartości
 * Przypisanie wartości stanu gry jako Nowa gra, Gra w Trakcie oraz Zakończona Rozgrywka
 */
public enum GameStatus {
    NEW, IN_PROGRESS, FINISHED
}
