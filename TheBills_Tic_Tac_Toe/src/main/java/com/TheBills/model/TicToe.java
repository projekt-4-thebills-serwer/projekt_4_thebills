package com.TheBills.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *  Enum to typ wyliczeniowy, który umożliwia w Javie zadeklarowanie ograniczonej liczby możliwych wartości
 *  Przypisanie wartości odpowiednim znakom, w tym wypadku X=1, O=2
 */
@AllArgsConstructor
@Getter
public enum TicToe {
    X(1), O(2);

    private Integer value;
}
