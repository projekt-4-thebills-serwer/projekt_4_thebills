package com.TheBills.model;

import lombok.Data;

/**
 * Metoda przyjmująca login jako String
 */
@Data
public class Player {

    private String login;
}
