package com.TheBills.controller.dto;

import com.TheBills.model.Player;
import lombok.Data;
/**
 * Klasa ConnectRequest odpowiada za połączenie dwóch graczy do rozgrywki
 * Jest ona wykorzystywana w klasie GameController
 */

@Data
public class ConnectRequest {
    private Player player;
    private String gameId;
}
