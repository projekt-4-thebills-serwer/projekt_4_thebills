package com.TheBills.controller;
/**
 *  @PostMapping - adnotacja mapująca żądania HTTP POST na określone metody obsługi.
 *  @RequestMapping – adnotacja wskazującą, że dana metoda stanowi Endpoint.
 *  adnotacja ta może być wywoływana zdalnie. Przyjmuje ona wartość stanowiącą relatywną ścieżkę dla jej wywołania
 *  @AllArgsConstructor - generuje konstruktor z jednym parametrem dla każdego pola w klasie
 *  @RequestBody - Treść żądania to dane wysyłane przez klienta do interfejsu API. Treść odpowiedzi to dane, które interfejs API wysyła do klienta
 *  @RestController - łączy @Controller i @ResponseBody, co eliminuje potrzebę dodawania adnotacji do każdej metody obsługi żądań klasy kontrolera za pomocą adnotacji @ResponseBody.
 *  @Slf4j - umożliwia logowanie zdarzeń występujących w trakcie działania aplikacji
 */

import com.TheBills.controller.dto.ConnectRequest;
import com.TheBills.exception.InvalidGameException;
import com.TheBills.exception.InvalidParamException;
import com.TheBills.exception.NotFoundException;
import com.TheBills.model.Game;
import com.TheBills.model.GamePlay;
import com.TheBills.model.Player;
import com.TheBills.service.GameService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Klasa ta zapewnia możliwość komunikacji z serwerem poprzez endpoint'y
 */

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/game")
public class GameController {
    private final GameService gameService;
   private final SimpMessagingTemplate simpMessagingTemplate;
    /**
     *Stworzenie gry
     */
    @PostMapping("/start")
    public ResponseEntity<Game> start(@RequestBody Player player) {
        log.info("start game request: {}", player);
        return ResponseEntity.ok(gameService.createGame(player));
    }
    /**
     *Połączenie z grą
     */
    @PostMapping("/connect")
    public  ResponseEntity<Game> connect(@RequestBody ConnectRequest request) throws InvalidParamException, InvalidGameException {
        log.info("connect request: {}",request);
        return  ResponseEntity.ok(gameService.connectToGame(request.getPlayer(), request.getGameId()));

    }
    /**
     * Połączenie z losową grą
     */
    @PostMapping("/connect/random")
    public ResponseEntity<Game> connectRandom(@RequestBody Player player) throws NotFoundException {
        log.info("connect random {}", player);
        return ResponseEntity.ok(gameService.connectToRandomGame(player));
    }
    /**
     *Aktualna rozgrywka
     */
    @PostMapping("/gameplay")
    public ResponseEntity<Game> gamePlay(@RequestBody GamePlay request) throws NotFoundException, InvalidGameException {
        log.info("gameplay: {}", request);
        Game game = gameService.gamePlay(request);
        simpMessagingTemplate.convertAndSend("/topic/game-progress/" + game.getGameId(), game);
        return ResponseEntity.ok(game);
    }
}
